const fs = require("fs");
global.$ = {
  gulp: require("gulp"),
  path: {
    tasks: require("./gulp/config/task.js"),
  },
};

$.path.tasks.forEach(function (taskPath) {
  console.log(taskPath);
  require(taskPath)();
});

$.gulp.task(
  "build",
  $.gulp.series(
    "clean",
    "sprite",
    "images:build",
    $.gulp.parallel([
      "sass:build",
      "scripts:build",
      "fonts",
      "fontawesomefonts",
      "fontawesomecss",
      "svg",
    ])
  )
);

$.gulp.task(
  "default",
  $.gulp.series(
    "sprite",
    "images:copy",
    "fonts",
    "fontawesomefonts",
    "fontawesomecss",
    $.gulp.parallel("watch")
  )
);
