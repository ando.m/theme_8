"use strict";

const config = require("../config/config.js");
const sass = require("gulp-sass")(require("sass"));
const sourcemaps = require("gulp-sourcemaps");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const postcssCustomMedia = require("postcss-custom-media");
const utils = require("postcss-utilities");

const watchPlugins = [autoprefixer(), postcssCustomMedia(), utils()];

const buildPlugins = [autoprefixer(), postcssCustomMedia(), utils(), cssnano()];

module.exports = function () {
  $.gulp.task("sass", function () {
    return $.gulp
      .src(config.sassMain)
      .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(postcss(watchPlugins))
      .pipe(sourcemaps.write())
      .pipe($.gulp.dest(config.cssOut));
  });
  $.gulp.task("sass:build", function () {
    return $.gulp
      .src(config.sassMain)
      .pipe(gulpSass())
      .pipe(postcss(buildPlugins))
      .pipe($.gulp.dest(config.cssOut));
  });
};
