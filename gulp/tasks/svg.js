const svgmin = require("gulp-svgmin");
const cheerio = require("gulp-cheerio");
const replace = require("gulp-replace");
const svgSprite = require("gulp-svg-sprites");
const config = require("../config/config.js");

module.exports = function () {
  $.gulp.task("svg", function () {
    return (
      $.gulp
        .src(config.svg)
        .pipe(
          svgSprite({
            svg: {
              sprite: "sprite.svg",
            },
            mode: "symbols",
            svgPath: "%f",
            common: "ic",
          })
        )
        // TODO не работает удаление аттрибутов
        // .pipe(
        //   cheerio({
        //     run: function ($) {
        //       $("[fill]").removeAttr("fill");
        //       $("[stroke]").removeAttr("stroke");
        //       $("[style]").removeAttr("style");
        //     },
        //     parserOptions: { xmlMode: true },
        //   })
        // )
        .pipe(replace("&gt;", ">"))
        .pipe(replace("&quot;", " "))
        .pipe(replace("&apos;", "'"))
        .pipe($.gulp.dest(config.svgOut))
    );
  });
};
