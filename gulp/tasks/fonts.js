'use strict';
const config = require('../config/config.js');

module.exports = function(){
  $.gulp.task('fonts',function(){
    return $.gulp.src(config.src + '/fonts/**/*')
      .pipe($.gulp.dest(config.dist + '/fonts'))
  });
}
