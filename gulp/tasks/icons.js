'use strict';
const tinypng = require('gulp-tinypng-compress');
const spritesmith = require('gulp.spritesmith');
const config = require('../config/config.js');

module.exports = function(){
	$.gulp.task('sprite', function (files) {
		let spriteData =
      $.gulp.src(config.pngIcons) // source path of the sprite images
      .pipe(spritesmith({
      	imgName: 'sprite.png',
      	cssName: 'sprite.css',
      	imgPath: '../images/sprite.png'
      }));
  spriteData.img.pipe($.gulp.dest(config.dist + '/images/')); // output path for the sprite
  spriteData.css.pipe($.gulp.dest(config.dist + '/css/')); // output path for the CSS
  return spriteData;
});


$.gulp.task('sprite:build',function(){
  return $.gulp.src(config.dist + '/images/sprite.png')
      .pipe(tinypng({
        key: 'fCqEOwAjE-xjBf-xYe0VWLCvPQnco849',
        sigFile: 'images/.tinypng-sigs',
        log: true
      }))
        .pipe($.gulp.dest('./dist/images/'))
});
}

