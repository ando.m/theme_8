'use strict';
const config = require('../config/config.js');
const sourcemaps = require('gulp-sourcemaps');
const babel = require("gulp-babel");
const concat = require("gulp-concat");
module.exports = function(){
	$.gulp.task('scripts',function(){
		return $.gulp.src(config.jsFiles)
		.pipe(sourcemaps.init())
		.pipe(babel())
		.pipe(concat("app.js"))
		.pipe(sourcemaps.write())
		.pipe($.gulp.dest(config.jsOut))
	});
	$.gulp.task('scripts:build',function(){
		return $.gulp.src(config.jsFiles)
		.pipe(babel())
		.pipe(concat("app.js"))
		.pipe($.gulp.dest(config.jsOut))
	});
	$.gulp.task('scripts_apart',function(){
		return $.gulp.src(config.jsFiles)
		.pipe(babel())
		.pipe($.gulp.dest(config.jsOut))
	});
	$.gulp.task('scripts_apart:build',function(){
		return $.gulp.src(config.jsFiles)
		.pipe(babel())
		.pipe($.gulp.dest(config.jsOut))
	});

}