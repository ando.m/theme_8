'use strict';
const config = require('../config/config.js');
const rename = require("gulp-rename");

module.exports = function(){
  $.gulp.task('fontawesomefonts',function(){
    return $.gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/*')
      .pipe($.gulp.dest(config.dist + '/webfonts'))
  });
  $.gulp.task('fontawesomecss',function(){
    return $.gulp.src('./node_modules/@fortawesome/fontawesome-free/css/all.css')
      .pipe(rename({
        basename: "fontawesome",
      }))
      .pipe($.gulp.dest(config.dist + '/css'))
  });
};

