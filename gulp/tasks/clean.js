'use strict';

const clean = require('gulp-clean');
const config = require('../config/config.js');

module.exports = function(){
$.gulp.task('clean', function () {
    return $.gulp.src(config.dist, {read: false})
        .pipe(clean());
});

};