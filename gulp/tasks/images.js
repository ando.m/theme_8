'use strict';
const tinypng = require('gulp-tinypng-compress');
const config = require('../config/config.js');



module.exports = function(){
	$.gulp.task('images:build', function () {
  return $.gulp.src(config.images)
      .pipe(tinypng({
        key: 'fCqEOwAjE-xjBf-xYe0VWLCvPQnco849',
        sigFile: 'images/.tinypng-sigs',
        log: true
      }))
      .pipe($.gulp.dest(config.dist + '/images'));
});
 $.gulp.task('images:copy', function () {
 	return $.gulp.src(config.images)
 	.pipe($.gulp.dest(config.dist + '/images'));
 });

}