"use strict";
module.exports = {
  src: "./src",
  dist: "./dist",
  sassFiles: "./src/sass/**/*.scss",
  svg: "./src/svg/*.svg",
  svgOut: "./dist/svg",
  sassMain: "./src/sass/app.scss",
  cssOut: "./dist/css/",
  jsFiles: "./src/scripts/*.js",
  jsOut: "./dist/scripts",
  pngIcons: "./src/iconsSet/icons/*.png",
  images: "./src/images/*.{png,jpg,jpeg}",
};
