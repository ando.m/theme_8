module.exports = [
  "./gulp/tasks/sass",
  "./gulp/tasks/scripts",
  "./gulp/tasks/watch",
  "./gulp/tasks/icons",
  "./gulp/tasks/images",
  "./gulp/tasks/clean",
  "./gulp/tasks/fonts",
  "./gulp/tasks/fontawesome",
  "./gulp/tasks/svg",
];
